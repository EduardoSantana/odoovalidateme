<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Art extends Model
{
    protected $fillable = [
    	'Artist',
    	'Title',
    	'Description',
    	'CreateDate',
    	'ExibitDate'
    ];
}
